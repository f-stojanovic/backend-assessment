<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * A post
 *
 * @ApiResource(normalizationContext={"groups"={"post"}})
 * @ORM\Entity
 */
class Post
{

    /**
     * @var int The id of this post.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"post"})
     */
    private $id;

    /**
     * @var string Title of this post
     *
     * @ORM\Column(type="text")
     * @Groups({"post"})
     */
    public $title;

    /**
     * @var string Content of this post
     *
     * @ORM\Column(type="text")
     * @Groups({"post"})
     */
    public $content;

    /**
     * @var string Creator of this post
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @Groups({"post"})
     */
    public $creator;

    public function getId(): ?int
    {
        return $this->id;
    }

}




